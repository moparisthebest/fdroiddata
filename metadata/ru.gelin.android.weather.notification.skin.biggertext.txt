Categories:Internet
License:GPLv2
Web Site:https://code.google.com/p/weather-notification-android
Source Code:https://code.google.com/p/weather-notification-android/source
Issue Tracker:

Name:Weather Skin: Bigger Text
Auto Name:Weather notification skin: bigger text
Summary:Theme for Weather Notification
Description:
Big text skin for [[ru.gelin.android.weather.notification]].
.

Repo Type:srclib
Repo:WeatherNotification

Build:0.3.6,17
    commit=v.0.3.6
    subdir=skins/bigger-text

Auto Update Mode:Version v.%v
Update Check Mode:Tags
Current Version:0.3.6
Current Version Code:17

