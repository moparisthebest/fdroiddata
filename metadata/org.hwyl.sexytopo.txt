Categories:Science & Education
License:GPLv3
Web Site:
Source Code:https://github.com/richsmith/sexytopo
Issue Tracker:https://github.com/richsmith/sexytopo/issues

Auto Name:SexyTopo
Summary:Assist in cave surveying
Description:
Assists in cave surveying. It interfaces with an automated measuring device (the
Disto-X) and builds up an accurate model of the cave. It also provides a
sketching environment for the cave surveyor to fill out the details.
.

Repo Type:git
Repo:https://github.com/richsmith/sexytopo

Build:1.0.5,6
    disable=https://github.com/richsmith/sexytopo/issues/6
    commit=28b58408856d1a754b954a65c35d222874ca4d92
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.5
Current Version Code:6

