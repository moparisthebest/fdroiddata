Categories:Internet,System
License:GPLv3
Web Site:https://github.com/de-live-gdev/kimai-android/blob/HEAD/README.md
Source Code:https://github.com/de-live-gdev/kimai-android
Issue Tracker:https://github.com/de-live-gdev/kimai-android/issues

Auto Name:Kimai
Summary:Wrapper for Kimai
Description:
Client for the time managment software [http://www.kimai.org/ Kimai]. This app
makes mobile tracking and logging into your kimai existing installation easier.
If you want to be auto logged in after application start check the appropiate
box.

Normally you have to open your browser, go to favourites, click your kimai page,
input data. Now you only have to do the last step every time you want to add
entries.
.

Repo Type:git
Repo:https://github.com/de-live-gdev/kimai-android.git

Build:1.1,7
    commit=v1.1

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1
Current Version Code:7

