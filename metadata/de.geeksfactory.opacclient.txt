Categories:Reading
License:MIT
Web Site:http://opacapp.de
Source Code:https://github.com/raphaelm/opacclient
Issue Tracker:https://github.com/raphaelm/opacclient/issues

Auto Name:Web Opac
Summary:German public libraries
Description:
Client for some German, Austrian and Swiss public libraries that offer online
catalogues.
.

Repo Type:git
Repo:https://github.com/raphaelm/opacclient.git

Build:2.0.18,54
    commit=2.0.18
    submodules=yes
    init=rm -rf 3dparty/SlidingMenu/example && \
        (cd 3dparty/HoloEverywhere && \
        git submodule update --init && \
        $$SDK$$/tools/android update project -p contrib/ActionBarSherlock/actionbarsherlock)
    update=.,3dparty/HoloEverywhere/library,3dparty/SlidingMenu/library

Build:2.0.30,66
    commit=2.0.30
    submodules=yes
    init=rm -rf 3dparty/SlidingMenu/example && \
        (cd 3dparty/HoloEverywhere && \
        git submodule update --init && \
        $$SDK$$/tools/android update project -p contrib/ActionBarSherlock/actionbarsherlock)

Build:2.0.31,68
    commit=2.0.31
    submodules=yes
    prebuild=rm -rf 3dparty/SlidingMenu/example

Build:2.0.32,69
    commit=2.0.32
    submodules=yes
    prebuild=rm -rf 3dparty/SlidingMenu/example

Build:2.1.0,70
    commit=2.1.0
    submodules=yes
    prebuild=rm -rf 3dparty/SlidingMenu/example

Build:3.0.1,74
    commit=3.0.1
    submodules=yes

Build:3.0.3,76
    commit=3.0.3
    submodules=yes

Build:3.0.4,77
    commit=3.0.4
    submodules=yes

Build:3.0.5,79
    commit=3.0.5
    submodules=yes

Build:3.0.7,81
    commit=3.0.7
    submodules=yes

Build:3.0.8,82
    commit=3.0.8
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3
    rm=libs/jsoup-*,libs/acra-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/

Build:3.0.11,85
    commit=3.0.11
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.1.0,86
    commit=3.1.0
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.1.1,87
    commit=3.1.1
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.1.2,88
    commit=3.1.2
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.1.3,89
    commit=3.1.3
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.2.0,90
    commit=3.2.0
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.2.1,91
    commit=3.2.1
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.2.2,92
    commit=3.2.2
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.0,93
    commit=3.3.0
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.1,95
    commit=3.3.1
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.2,96
    commit=3.3.2
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.3,97
    commit=3.3.3
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.4,98
    commit=3.3.4
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.5,99
    commit=3.3.5
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.6,100
    commit=3.3.6
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.7,102
    commit=3.3.7
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.8,103
    commit=3.3.8
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.9,104
    commit=3.3.9
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:3.3.10,105
    commit=3.3.10
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:4.0.3,109
    disable=project structure changed
    commit=4.0.3
    submodules=yes
    srclibs=ACRA@acra-4.5.0,JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=libs/jsoup-*,libs/acra-*,libs/endless-*,libs/adapter-*,libs/cwac-*
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.6.3.jar libs/ && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.5.0.jar libs/ && \
        cp -fR $$Endless$$/src/com src/ && \
        cp -fR $$CWACAdapter$$/src/com src/

Build:4.0.4,110
    commit=4.0.4
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.0.6,112
    commit=4.0.6
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.0,113
    commit=4.1.0
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.2,115
    commit=4.1.2
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.3,116
    commit=4.1.3
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.4,117
    commit=4.1.4
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.5,118
    commit=4.1.5
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.6,119
    commit=4.1.6
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.7,120
    commit=4.1.7
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.8,121
    commit=4.1.8
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.10,124
    commit=4.1.10
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.1.11,125
    commit=4.1.11
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.2.0,126
    commit=4.2.0
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.2.1,127
    commit=4.2.1
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Build:4.3.0,128
    commit=4.3.0
    subdir=opacclient/opacapp
    submodules=yes
    gradle=yes
    srclibs=JSoup@jsoup-1.6.3,Endless@v1.2.3,CWACAdapter@v1.0.1
    rm=opacclient/opacapp/libs/*,opacclient/meaningdetector/libs/*
    prebuild=cp -fR $$Endless$$/src/com src/main/java/ && \
        cp -fR $$CWACAdapter$$/src/com src/main/java/ && \
        sed -i -e '/sonatype/d' ../build.gradle && \
        sed -i -e '/endless/d' -e '/adapter/d' build.gradle && \
        sed -i -e '/fileTree/acompile "org.bouncycastle:bcprov-jdk15on:1.51"' -e '/fileTree/acompile "org.apache.directory.studio:org.apache.commons.codec:1.6"' -e '/fileTree/acompile "commons-logging:commons-logging:1.1.3"' -e '/fileTree/acompile "org.apache.httpcomponents:fluent-hc:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpclient-cache:4.3.3"' -e '/fileTree/acompile "org.apache.httpcomponents:httpcore:4.3.2"' -e '/fileTree/acompile "org.apache.httpcomponents:httpmime:4.3.3"' -e '/fileTree/acompile "org.json:json:20141113"' ../meaningdetector/build.gradle

Maintainer Notes:
4.0.4+ shouldn't need gradle changes for meaningdetector, see
https://github.com/raphaelm/opacclient/issues/296 .
.

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:4.3.0
Current Version Code:128

