Categories:Reading,Multimedia
License:MIT
Web Site:https://github.com/nkanaev/bubble/blob/HEAD/README.md
Source Code:https://github.com/nkanaev/bubble
Issue Tracker:https://github.com/nkanaev/bubble/issues

Auto Name:Bubble
Summary:View comic books
Description:
* Optimized for tablets
* Supports CBZ/ZIP and CBR/RAR
* Advanced zoom and scaling modes
* Library management with automatic bookmarks
* Simple file browser
.

Repo Type:git
Repo:https://github.com/nkanaev/bubble.git

Build:1.0.2,1
    commit=v1.0.2
    subdir=app
    gradle=yes

Build:1.1.0,2
    commit=v1.1.0
    subdir=app
    gradle=yes

Build:1.2.0,3
    commit=v1.2.0
    subdir=app
    gradle=yes

Build:1.3.0,4
    commit=v1.3.0
    subdir=app
    gradle=yes

Maintainer Notes:
UCM:Tags wont work, since upstream uses a dedicated versions.gradle which we don't parse.
If this might be fixed, we can set AUM, too.
.

Auto Update Mode:None
Update Check Mode:None
Current Version:1.3.0
Current Version Code:4

